from mdtable import MDTable

from logo import print_logo
from style import print_style

markdown = MDTable('tmp/main-out.csv')
markdown_string_table = markdown.get_table()
markdown.save_table('tmp/main-out.md')

import markdown
table_html=markdown.markdown(markdown_string_table, extensions=['markdown.extensions.tables'])
print("""<html><head>""")
print_style()
print("""
<link rel="stylesheet" href="https://assets.ubuntu.com/v1/vanilla-framework-version-3.6.1.min.css" />
</head>
<body>
    <div style="margin-top: 10px;margin-bottom: 10px;padding-left: 1.5em;">""")

print_logo()

print("""<div style="
    position: absolute;
    left: 50%;
    top: 1.5rem;
    transform: translateX(-50%);
"><h3 style="">Riepilogo</h3></div><div style="
    display: inline-block;
    vertical-align: top;
    position: absolute;
    right: 2em;
"><small><i>Aggiornato: """)
from datetime import date

today = date.today()
print(today.strftime("%B %d, %Y"))
print("""</i></small></div></div>""")
print(table_html)

print("""
</body>
</html>
""")