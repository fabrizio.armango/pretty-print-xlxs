def print_style():
    print("""<style>
        @media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
        body table {
            border-top: 1px solid rgba(0,0,0,.15);
        }
        /*GIORNO*/
        table tr th:first-child,
        table tr td:first-child {
            width: 95px;
            vertical-align: middle;
            text-align: center;
            border-right: 1px solid rgba(0,0,0,.1);
        }
        /*SERVIZIO*/
        table tr th:nth-child(2),
        table tr td:nth-child(2) {
            border-right: 1px solid rgba(0,0,0,.1);
            width: 220px;
        }
        /*PICK UP*/
        table tr th:nth-child(3),
        table tr td:nth-child(3) {
            border-right: 1px solid rgba(0,0,0,.1);
        }
        /*PAX*/
        table tr th:nth-child(4),
        table tr td:nth-child(4) {
            border-right: 1px solid rgba(0,0,0,.1);
            width: 40px;
            text-align: center;
        }
        /*PERSON*/
        table tr th:last-child,
        table tr td:last-child {
            text-align: center;
        }
    </style>
    """)