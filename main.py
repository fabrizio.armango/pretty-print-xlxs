import os
import openpyxl
from pathlib import Path
import re

xlsx_file = Path('tmp/input.xlsx')
wb_obj = openpyxl.load_workbook(xlsx_file)
sheet = None
try:
    sheet = wb_obj.get_sheet_by_name(os.environ.get('PAGE_TITLE'))
except KeyError:
    raise Exception("Sheet not found. Available sheet names are", wb_obj.sheetnames)

ONLY_ASSISTANCE=True if os.environ.get('ONLY_ASSISTANCE') == "true" else False
ONLY_GUIDE=True if os.environ.get('ONLY_GUIDE') == "true" else False
if ONLY_ASSISTANCE:
    print("Giorno,Servizio,Pick Up, Pax,Holder,Assistente")
else:
    print("Giorno,Servizio,Pick Up, Pax,Holder,Guida")

row_num = 1
for row in sheet.iter_rows():
    row = str(row_num)
    date = str(sheet["A" + row].value).replace(" 00:00:00", "")
    service_name = sheet["E" + row].value.lower()
    holder = sheet["K" + row].value
    destination_area = sheet["H" + row].value.lower()
    pax = sheet["L" + row].value
    language = sheet["N" + row].value.lower()
    pick_up = sheet["G" + row].value

    # print("date: " + date + "\t" + service_name + "\t" + language)

    if not ONLY_ASSISTANCE:
        is_pompei_tour = False
        if re.search("pompei", service_name):
            #  "potrebbe essere un servizio a Pompei")
            if not (re.search("drop off pompei", service_name)):
                # è un servizio a pompei
                is_pompei_tour = True 
        
        if is_pompei_tour:             
            print('"<small>' + date + '</small>"', end=",")
            
            """ The following emojis have been commented
            because they aren't correctly rendered in print mode.
            """
            # if language == "spagnolo": 
            #     print("🇪🇸 ", end="")
            # if language == "inglese":
            #     print("🇬🇧 ", end="")
            print('"Guida a Pompei in', end=" ")
            print(language + '"', end=",")
            print('"<small>' + pick_up + '</small>"', end=",")
            print('"' + str(pax) + '"', end=",")
            print('"' + holder + '"', end=",\n")
    if not ONLY_GUIDE:
        is_assistance = False
        if re.search("italo", destination_area):
            is_assistance = True
        if re.search("tba", destination_area):
            is_assistance = True

        if is_assistance:
            print('"<small>' + date + '</small>"', end=",")
            print('"Assistenza in partenza<br/><small>' + destination_area + '</small>"', end=",")
            print('"<small>' + pick_up + '</small>"', end=",")
            print('"' + str(pax) + '"', end=",")
            print('"' + holder + '"', end=",\n")

    row_num = row_num + 1