# pretty-print-xlxs
## Description
pretty-print-xlxs is a simple tool to:

- download an xlxs file from a sharepoint (URL must be defined in .env as INPUT_FILE_URL)
- transform it to markdown format
- transform the markdown to an html styled using the vanilla framework.
- print styled html file to a pdf file using chromium headless


## Print the pdf
`./print.sh`

## Change logo
Edit the print_logo function defined in logo.py
> the function must simply print the html.

``` python
def print_logo():
    print("""<div>
        <img src""/>
    </div>""")
```


## Edit column size or add new styles
edit or add new css rules in style.py 
