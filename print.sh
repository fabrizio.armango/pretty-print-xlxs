#!/bin/sh

export PAGE_TITLE='SETTEMBRE al 15-09 '
mkdir tmp
mkdir out
mkdir logs

set -e
set -a; . ./.env; set +a

wget \
    $INPUT_FILE_URL \
    -O tmp/input.xlsx

month_name=`date +%b`
ONLY_GUIDE=true python3 main.py > tmp/main-out.csv 
now=`date +"%Y-%m-%d_%H-%M-%S"`
cp tmp/main-out.csv "logs/tour-${now}.csv"
python3 csv2md.py > tmp/tour-$month_name.html
chromium \
    --headless \
    --print-to-pdf="out/tour-${month_name}.pdf" \
    --print-to-pdf-no-header \
    --virtual-time-budget=10000 \
    --no-margins \
    --disable-gpu \
    tmp/tour-$month_name.html

ONLY_ASSISTANCE=true python3 main.py > tmp/main-out.csv
cp tmp/main-out.csv "logs/assistenze-${now}.csv"
python3 csv2md.py > tmp/assistenze-$month_name.html
chromium \
    --headless \
    --print-to-pdf="out/assistenze-${month_name}.pdf" \
    --print-to-pdf-no-header \
    --virtual-time-budget=10000 \
    --no-margins \
    --disable-gpu \
    tmp/assistenze-$month_name.html